const jwt = require("jsonwebtoken");
const secret = "Capstone2";

// Token Creation
module.exports.createAccessToken = (user) => {
  console.log(user);
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
  }
  return jwt.sign(data, secret, {});
}

// Token Verification
module.exports.verify = (request, response, next) =>{
  let token = request.headers.authorization;
  if (token !== undefined){
    token = token.slice(7, token.length);
    console.log(token);
    return jwt.verify(token, secret, (error, data) => {
      if(error){
        return response.send({
          auth: "Token Invalid!"
        });
      }
      else{
        request.userData = data;
        console.log(request.userData)
        next();
      }
    })
  }
  else{
    response.send({
      message: "Auth failed. No Token Provided!"
    })
  }
}

// Token Decryptor
module.exports.decode = (token) => {
  if(token !== undefined){
    token = token.slice(7, token.length);

    return jwt.verify(token, secret, (error, data) => {
      if(error){
        return null;
      }
      else{
        return jwt.decode(token, {complete:true}).payload;
      }
    })
  }
  else{
    return null;
  }
}