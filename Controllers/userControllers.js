const User = require("../Models/user");
const Product = require("../Models/product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const product = require("../Models/product");

module.exports.checkEmailExists = (request, response) => {
  return User.find({email: request.body.email}).then(result => {
    console.log(result);
    if(result.length > 0) {
      return response.send(true);
    }
    else{
      return response.send(false);
    }
  })
  .catch(error => response.send(error));
};

// User Registration

module.exports.registerUser = (request, response) => {
  console.log(request.body);
  User.findOne({email: request.body.email}).then(result => {
    console.log(result);
    if(result != null && result.email == request.body.email){
      return response.send("Email is Already used! Please use a different Email Address to Create an Account.")
    }
    else{
      let newUser = new User ({
        firstName: request.body.firstName,
        lastName: request.body.lastName,
        email: request.body.email,
        password: bcrypt.hashSync(request.body.password, 10),
        mobileNo: request.body.mobileNo
      })
      console.log(newUser);
    
      return newUser.save()
      .then(user => {
        console.log(user)
        response.send(true)
      })
      .catch(error => {
        console.log(error);
        response.send(false)
      })
    }
  })
};

module.exports.loginUser = (request, response) => {
  return User.findOne({email: request.body.email}).then(result => {
    if(result == null){
      // return response.send(false)
      return response.status(401).send("No Account Found! Please check your Credentials.")
    }
    else{
      const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password)
      if(isPasswordCorrect){
        return response.send({AccessToken: auth.createAccessToken(result)});
      }
      else{
        // return response.send(false);
        return response.status(401).send("Incorrect Password! Please try again.")
      }
    }
  })
}
// Add to Cart
/* 
module.exports.addToCart = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  return Product.findById(request.params.productId).then(result => {
    let cartData = {
      productName: request.body.productName,
      quantity: request.body.quantity,
      price: request.body.price
    }
    console.log(cartData);
  })
}
 */
// Checkout Order

module.exports.checkout = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  let productName = await Product.findById(request.body.productId).then(result => result.productName)
  let data = {
    userId: userData.id,
    email: userData.email,
    productId: request.body.productId,
    quantity: request.body.quantity,
    productName: productName
  }
  console.log(data);
  
  let amount = 0;
  let price = await Product.findById(data.productId).then(result => {
    amount = result.price
    return amount
  })
  
  // console.log("This is the price of your purchase " + price)
  
  let isUserUpdated = await User.findById(data.userId).then(user => {
    user.orders.push({
      totalAmount: price*data.quantity,
      products: {
        productId: data.productId,
        productName: data.productName,
        quantity: data.quantity
      }
    })
    return user.save().then(result => {
      console.log(result)
      return true
    })
    .catch(error => {
      console.log(error)
      return false;
    })
  })
  console.log(isUserUpdated);

  let isProductUpdated = await Product.findById(data.productId).then(product => {
    product.orders.push({
      orderId: data.orderId,
      userId: data.userId,
      // quantity: data.quantity
    })
    product.quantity -= data.quantity;
    return product.save().then(result => {
      console.log(result);
      return true
    })
    .catch(error => {
      console.log(error);
      return false
    })
  })
  console.log(isProductUpdated);
  (isUserUpdated && isProductUpdated) ? response.send(true) : response.send(false)
}

/* 
module.exports.checkout = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  let productName = await Product.findById(request.body.productId).then(result => result.productName)
  let data = {
    userId: userData.id,
    email: userData.email,
    productId: request.body.productId,
    // quantity: request.body.quantity,
    productName: productName
  }
  console.log(data);
  
  let isUserUpdated = await User.findById(data.userId).then(user => {
    user.orders.push({
      productId: data.productId,
      productName: data.productName,
      quantity: data.quantity
    })
    return user.save().then(result => {
      console.log(result)
      return true
    })
    .catch(error => {
      console.log(error)
      return false;
    })
  })
  console.log(isUserUpdated);

  let isProductUpdated = await Product.findById(data.productId).then(product => {
    product.orders.push({
      orderId: data.orderId,
      userId: data.userId,
      // quantity: data.quantity
    })
    product.quantity -= 1;
    return product.save().then(result => {
      console.log(result);
      return true
    })
    .catch(error => {
      console.log(error);
      return false
    })
  })
  console.log(isProductUpdated);
  (isUserUpdated && isProductUpdated) ? response.send(true) : response.send(false)
}
 */

// Retrieve User Details
module.exports.getProfile = (request, response) => {
  console.log(request.params.userId);
  return User.findById(request.params.userId).then(result => {
    response.send(result)
  })
}


// Find all User
module.exports.findAllUser = (request, response) => {
  return User.find({}).then(result => {
    response.send(result)
  })
  .catch(error => {
    response.send(error)
  })
}

// Delete A User
module.exports.deleteUser = (request, response) => {
  console.log(request.params.userId)
  return User.findByIdAndRemove(request.params.userId).then(result => {
    response.send(ressult)
  })
  .catch(error => {
    response.send(error)
  })
}