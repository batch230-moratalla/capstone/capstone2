const Product = require("../Models/product");
const auth = require("../auth");

/* 
module.exports.addProduct = (request, response) => {
  const userData = auth.decode(request.headers.authorizatin)
  let newProduct = new Product({
    productName:request.body.productName,
    description: request.body.description,
    price: request.body.price,
    quantity: request.body.quantity
  })
  if(request.userData.isAdmin){
    return newProduct.save()
    .then(product => {
      console.log(product)
      response.send(true)
    })
    .catch(error => {
      console.og(error)
      response.send(false)
    })
  }
  else{
    return response.status(401).send("You don't have access on this page!")
  }
}
 */

module.exports.addProduct = (request, response) => {
  const userData = auth.decode(request.headers.authorization)
  console.log(request.body)
  Product.findOne({productName: request.body.productName}).then(result => {
    console.log(result);
    if(result != null && result.productName == request.body.productName){
      return response.send("Duplicate Product Found!")
    }
    else{
      let newProduct = new Product({
        productName:request.body.productName,
        description: request.body.description,
        price: request.body.price,
        quantity: request.body.quantity
      })
      if(request.userData.isAdmin){
        return newProduct.save()
        .then(product => {
          console.log(product)
          response.send(true)
        })
        .catch(error => {
          console.og(error)
          response.send(false)
        })
      }
      else{
        return response.status(401).send("You don't have access on this page!")
      }
    }
  })
}

// Get all Products for admin
/* 
module.exports.allProducts = (request, response) => {
  const userData = auth.decode(request.headers.authorizatin);
  if(request.userData.isAdmin){
    return Product.find({}).then(result => {
      response.send(result)
    });
  }
  else{
    return response(false)
  };
};
 */

// Get All Products None Admin
module.exports.allProducts = (request, response) => {
  return Product.find({}).then(result => {
    response.send(result)
  })
}

// Retrieve all Active Products
module.exports.allActiveProducts = (request, response) =>{
	return Product.find({isActive: true}).then(result => {
    response.send(result)
  })
}


// Retrieve a Product
module.exports.getProduct = (request, response) => {
  console.log(request.params.productId);
  return Product.findById(request.params.productId).then(result => {
    response.send(result)
  })
}

// Update a Product
module.exports.updateAproduct = (request, response) => {
  const userData = auth.decode(request.headers.authorization)
  if(userData.isAdmin){
    let updateProduct = {
      productName: request.body.productName,
      description: request.body.description,
      price: request.body.price,
      quantity: request.body.quantity
    }
    return Product.findByIdAndUpdate(request.params.productId, updateProduct, {new:true}).then(result => {
      console.log(result)
      // response.send(result)
      response.send("Product is now Updated!")
    })
    .catch(error => {
      console.log(error)
      response.send(false)
    })
  }
  else{
    return response.status(401).send("You don't have access on this page!")
  }
};

// Archive a Product
module.exports.archiveProduct = (request, response) => {
  const userData = auth.decode(request.headers.authorization)
  let changeActiveProduct = {
    isActive: request.body.isActive
  }
  if(userData.isAdmin){
    return Product.findByIdAndUpdate(request.params.productId, changeActiveProduct).then(result => {
      // console.log(result)
      // response.send(true)
      response.send("Product is now move to Archive")
    })
    .catch(error => {
      console.log(error)
      response.send(false)
    })
  }
  else{
    return response.status(401).send("You don't have access to this page!");
  }
}

// Delete A User
module.exports.deleteProduct = (request, response) => {
  console.log(request.params.productId)
  return Product.findByIdAndRemove(request.params.productId).then(result => {
    response.send(ressult)
  })
  .catch(error => {
    response.send(error)
  })
}