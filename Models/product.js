const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
  productName: {
    type: String,
    required: [true, "Product Name is required"]
  },
  description: {
    type: String,
    required: [true, "Description is required"]
  },
  price: {
    type: Number,
    required: [true, "Price is required"]
  },
  quantity: {
    type: Number,
    required: [true, "Number of items are required"]
  },
  isActive: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
    default: new Date()
  },
  orders: [
    {
      orderId: {
        type: String,
        // required: [true, "Order ID is required"]
      },
      userId: {
        type: String,
        required: [true, "User ID is required"]
      },
      userEmail: {
        type: String
      },
      quantity: {
        type: Number,
        // required: [true, "Quantity is required"]
      },
      purchasedOn: {
        type: Date,
        default: new Date()
      }
    }
  ]
});

module.exports = mongoose.model("Product", productSchema);