/* 
npm init -y - initialize
npm install express
npm install mongoose
npm install cors
npm install bcrypt
npm install jsonwebtoken
touch .gitignore
*/

const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./Routes/userRoutes");
const productRoutes = require("./Routes/productRoutes");
const cors = require("cors");
const app = express();
const port = process.env.PORT || 5000;

mongoose.connect("mongodb+srv://admin:admin@batch230.1dtuoqz.mongodb.net/Capstone-Project?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error!"));
db.once("open", () => console.log("You are now Connected to your Database Atlas!"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.listen(port, () => {console.log(`API is now online on port ${port}`)});