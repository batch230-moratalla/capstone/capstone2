const express = require("express");
const router = express.Router();
const productControllers = require("../Controllers/productControllers");
const auth = require("../auth");

// console.log(productControllers);

router.post("/create", auth.verify, productControllers.addProduct)

// router.get("/allProducts", auth.verify, productControllers.allProducts);
router.get("/allProducts", productControllers.allProducts);

router.get("/activeProducts", productControllers.allActiveProducts);

router.get("/:productId", productControllers.getProduct)

router.put("/:productId", auth.verify, productControllers.updateAproduct);

router.patch("/:productId", auth.verify, productControllers.archiveProduct);

router.delete("/:productId", productControllers.deleteProduct);

module.exports = router;