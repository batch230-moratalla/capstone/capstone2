const express = require("express");
const router = express.Router();
const userControllers = require("../Controllers/userControllers");
const auth = require("../auth");

// console.log(userControllers);

router.post("/checkEmail", userControllers.checkEmailExists);

router.post("/register", userControllers.registerUser);

router.post("/login", userControllers.loginUser);

router.get("/allUser", userControllers.findAllUser);

router.get("/:userId", userControllers.getProfile);

router.delete("/:userId", userControllers.deleteUser);

// router.post("/addToCart", userControllers.addToCart);

router.post("/order", auth.verify, userControllers.checkout);

module.exports = router;